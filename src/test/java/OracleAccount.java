import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;


public class OracleAccount {

    public WebDriver driver;
    public int tiempoMax=20;
    public String correo="tecGuru02@yopmaill.com";

    @Before
    public void setUp() throws Exception {

        String path = System.getProperty("user.dir");
        System.setProperty("webdriver.chrome.driver",path+"/src/main/resources/chromedriver.exe");

        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://profile.oracle.com/myprofile/account/create-account.jspx");
    }

    @After
    public void tearDown() throws Exception {
        driver.quit();
    }


    public void llenadoDeCampos() throws InterruptedException {
        Thread.sleep(2000);
        driver.findElement(By.xpath("//*[@id=\"email::content\"]")).sendKeys(correo);
        driver.findElement(By.xpath("//*[@id=\"password::content\"]")).sendKeys("Selenium02#");
        driver.findElement(By.xpath("//*[@id=\"retypePassword::content\"]")).sendKeys("Selenium02#");
        //Select drpCountry = new Select(driver.findElement(By.xpath("//*[@id=\"country::content\"]")));
        //Thread.sleep(50000);
        //drpCountry.selectByVisibleText("Mexico");
        driver.findElement(By.xpath("//*[@id=\"firstName::content\"]")).sendKeys("Itzel");
        driver.findElement(By.xpath("//*[@id=\"lastName::content\"]")).sendKeys("Miranda");
        driver.findElement(By.xpath("//*[@id=\"jobTitle::content\"]")).sendKeys("QA Agent");
        driver.findElement(By.xpath("//*[@id=\"workPhone::content\"]")).sendKeys("5501010101");
        driver.findElement(By.xpath("//*[@id=\"companyName::content\"]")).sendKeys("TecGurus");
        driver.findElement(By.xpath("//*[@id=\"address1::content\"]")).sendKeys("Calle #123 Mexico CDMX CP 01440");
        driver.findElement(By.xpath("//*[@id=\"city::content\"]")).sendKeys("Selenium02#");
        Select drpState = new Select(driver.findElement(By.xpath("//*[@id=\"state::content\"]")));
        drpState.selectByVisibleText("Chiapas");
        driver.findElement(By.xpath("//*[@id=\"postalCode::content\"]")).sendKeys("04100");
    }

    @Test
    public void caso1ValidaEmail() throws InterruptedException {
        llenadoDeCampos();
        driver.findElement(By.xpath("//*[@id=\"email::content\"]")).clear();
        driver.findElement(By.xpath("//*[@id=\"cb1\"]/a/span")).click();
        WebDriverWait wait = new WebDriverWait(driver,tiempoMax);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"afr::msgForemail\"]/table/tbody/tr[1]/td[2]")));
        WebElement emailmensaje= (WebElement)  driver.findElement(By.xpath("//*[@id=\"afr::msgForemail\"]/table/tbody/tr[1]/td[2]"));
        Assert.assertEquals(emailmensaje.getText(),"Email Address is required");
        driver.close();
    }

    @Test
    public void caso2ValidaPassword() throws InterruptedException {
        llenadoDeCampos();
        driver.findElement(By.xpath("//*[@id=\"password::content\"]")).clear();
        driver.findElement(By.xpath("//*[@id=\"cb1\"]/a/span")).click();
        WebDriverWait wait = new WebDriverWait(driver,tiempoMax);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"afr::msgForpassword\"]/table/tbody/tr[1]/td[2]")));
        WebElement emailmensaje= (WebElement)  driver.findElement(By.xpath("//*[@id=\"afr::msgForpassword\"]/table/tbody/tr[1]/td[2]"));
        Assert.assertEquals(emailmensaje.getText(),"Password is required");
        driver.close();
    }

    @Test
    public void caso3ValidaComfirmPassword() throws InterruptedException {
        llenadoDeCampos();
        driver.findElement(By.xpath("//*[@id=\"retypePassword::content\"]")).clear();
        driver.findElement(By.xpath("//*[@id=\"cb1\"]/a/span")).click();
        WebDriverWait wait = new WebDriverWait(driver,tiempoMax);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"afr::msgForretypePassword\"]/table/tbody/tr[1]/td[2]")));
        WebElement emailmensaje= (WebElement)  driver.findElement(By.xpath("//*[@id=\"afr::msgForretypePassword\"]/table/tbody/tr[1]/td[2]"));
        Assert.assertEquals(emailmensaje.getText(),"Retype password is required");
        driver.close();
    }

    @Test
    public void caso4ValidaPais() throws InterruptedException {
        llenadoDeCampos();
        Select drpCountry = new Select(driver.findElement(By.xpath("//*[@id=\"country::content\"]")));
        drpCountry.selectByVisibleText("-Select-");
        driver.findElement(By.xpath("//*[@id=\"cb1\"]/a/span")).click();
        WebDriverWait wait = new WebDriverWait(driver,tiempoMax);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"afr::msgForcountry\"]/table/tbody/tr[1]/td[2]")));
        WebElement emailmensaje= (WebElement)  driver.findElement(By.xpath("//*[@id=\"afr::msgForcountry\"]/table/tbody/tr[1]/td[2]"));
        Assert.assertEquals(emailmensaje.getText(),"Country is required");
        driver.close();
    }


    @Test
    public void caso5ValidaNombre() throws InterruptedException {
        llenadoDeCampos();
        driver.findElement(By.xpath("//*[@id=\"firstName::content\"]")).clear();
        driver.findElement(By.xpath("//*[@id=\"cb1\"]/a/span")).click();
        WebDriverWait wait = new WebDriverWait(driver,tiempoMax);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"afr::msgForfirstName\"]/table/tbody/tr[1]/td[2]")));
        WebElement emailmensaje= (WebElement)  driver.findElement(By.xpath("//*[@id=\"afr::msgForfirstName\"]/table/tbody/tr[1]/td[2]"));
        Assert.assertEquals(emailmensaje.getText(),"First or Given Name is required");
        driver.close();
    }

    @Test
    public void caso6ValidaApellido() throws InterruptedException {
        llenadoDeCampos();
        driver.findElement(By.xpath("//*[@id=\"lastName::content\"]")).clear();
        driver.findElement(By.xpath("//*[@id=\"cb1\"]/a/span")).click();
        WebDriverWait wait = new WebDriverWait(driver,tiempoMax);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"afr::msgForlastName\"]/table/tbody/tr[1]/td[2]")));
        WebElement emailmensaje= (WebElement)  driver.findElement(By.xpath("//*[@id=\"afr::msgForlastName\"]/table/tbody/tr[1]/td[2]"));
        Assert.assertEquals(emailmensaje.getText(),"Last Name is required");
        driver.close();
    }

    @Test
    public void caso7ValidaTrabajo() throws InterruptedException {
        llenadoDeCampos();
        driver.findElement(By.xpath("//*[@id=\"jobTitle::content\"]")).clear();
        driver.findElement(By.xpath("//*[@id=\"cb1\"]/a/span")).click();
        WebDriverWait wait = new WebDriverWait(driver,tiempoMax);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"afr::msgForjobTitle\"]/table/tbody/tr[1]/td[2]")));
        WebElement emailmensaje= (WebElement)  driver.findElement(By.xpath("//*[@id=\"afr::msgForjobTitle\"]/table/tbody/tr[1]/td[2]"));
        Assert.assertEquals(emailmensaje.getText(),"Job Title is required");
        driver.close();
    }

    @Test
    public void caso8ValidaTelTrabajo() throws InterruptedException {
        llenadoDeCampos();
        driver.findElement(By.xpath("//*[@id=\"workPhone::content\"]")).clear();
        driver.findElement(By.xpath("//*[@id=\"cb1\"]/a/span")).click();
        WebDriverWait wait = new WebDriverWait(driver,tiempoMax);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"afr::msgForworkPhone\"]/table/tbody/tr[1]/td[2]")));
        WebElement emailmensaje= (WebElement)  driver.findElement(By.xpath("//*[@id=\"afr::msgForworkPhone\"]/table/tbody/tr[1]/td[2]"));
        Assert.assertEquals(emailmensaje.getText(),"Work Phone is required");
        driver.close();
    }

    @Test
    public void caso9ValidaCompany() throws InterruptedException {
        llenadoDeCampos();
        driver.findElement(By.xpath("//*[@id=\"companyName::content\"]")).clear();
        driver.findElement(By.xpath("//*[@id=\"cb1\"]/a/span")).click();
        WebDriverWait wait = new WebDriverWait(driver,tiempoMax);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"afr::msgForcompanyName\"]/table/tbody/tr[1]/td[2]")));
        WebElement emailmensaje= (WebElement)  driver.findElement(By.xpath("//*[@id=\"afr::msgForcompanyName\"]/table/tbody/tr[1]/td[2]"));
        Assert.assertEquals(emailmensaje.getText(),"Company Name is required");
        driver.close();
    }

    @Test
    public void caso10ValidaDireccion() throws InterruptedException {
        llenadoDeCampos();
        driver.findElement(By.xpath("//*[@id=\"address1::content\"]")).clear();
        driver.findElement(By.xpath("//*[@id=\"cb1\"]/a/span")).click();
        WebDriverWait wait = new WebDriverWait(driver,tiempoMax);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"afr::msgForaddress1\"]/table/tbody/tr[1]/td[2]")));
        WebElement emailmensaje= (WebElement)  driver.findElement(By.xpath("//*[@id=\"afr::msgForaddress1\"]/table/tbody/tr[1]/td[2]"));
        Assert.assertEquals(emailmensaje.getText(),"Address is required");
        driver.close();
    }

    @Test
    public void caso11ValidaCiudad() throws InterruptedException {
        llenadoDeCampos();
        driver.findElement(By.xpath("//*[@id=\"city::content\"]")).clear();
        driver.findElement(By.xpath("//*[@id=\"cb1\"]/a/span")).click();
        WebDriverWait wait = new WebDriverWait(driver,tiempoMax);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"afr::msgForcity\"]/table/tbody/tr[1]/td[2]")));
        WebElement emailmensaje= (WebElement)  driver.findElement(By.xpath("//*[@id=\"afr::msgForcity\"]/table/tbody/tr[1]/td[2]"));
        Assert.assertEquals(emailmensaje.getText(),"City is required");
        driver.close();
    }

    @Test
    public void caso12ValidaEstado() throws InterruptedException {
        llenadoDeCampos();
        Select drpState = new Select(driver.findElement(By.xpath("//*[@id=\"state::content\"]")));
        drpState.selectByVisibleText("-Seleccionar-");
        driver.findElement(By.xpath("//*[@id=\"cb1\"]/a/span")).click();
        WebDriverWait wait = new WebDriverWait(driver,tiempoMax);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"afr::msgForstate\"]/table/tbody/tr[1]/td[2]")));
        WebElement emailmensaje= (WebElement)  driver.findElement(By.xpath("//*[@id=\"afr::msgForstate\"]/table/tbody/tr[1]/td[2]"));
        Assert.assertEquals(emailmensaje.getText(),"State/Province is required");
        driver.close();
    }

    @Test
    public void caso13ValidaCodigoPostal() throws InterruptedException {
        llenadoDeCampos();
        driver.findElement(By.xpath("//*[@id=\"postalCode::content\"]")).clear();
        driver.findElement(By.xpath("//*[@id=\"cb1\"]/a/span")).click();
        WebDriverWait wait = new WebDriverWait(driver,tiempoMax);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"afr::msgForpostalCode\"]/table/tbody/tr[1]/td[2]")));
        WebElement emailmensaje= (WebElement)  driver.findElement(By.xpath("//*[@id=\"afr::msgForpostalCode\"]/table/tbody/tr[1]/td[2]"));
        Assert.assertEquals(emailmensaje.getText(),"ZIP/Postal Code is required");
        driver.close();
    }

    @Test
    public void caso14CrearCuenta() throws InterruptedException {
        llenadoDeCampos();
        driver.findElement(By.xpath("//*[@id=\"cb1\"]/a/span")).click();
        WebDriverWait wait = new WebDriverWait(driver,tiempoMax);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"createaccount:dc12:content::content\"]/span[2]")));
        WebElement mensajeCuenta= (WebElement)  driver.findElement(By.xpath("//*[@id=\"createaccount:dc12:content::content\"]/span[2]"));
        WebElement confirmCorreo= (WebElement) driver.findElement(By.xpath("//*[@id=\"createaccount:dc12:content::content\"]/span[3]"));
        Assert.assertEquals(mensajeCuenta.getText(),"Verify your email address to use your account.");
        Assert.assertEquals(confirmCorreo.getText(),"We sent an email to "+correo+" with a button to verify your email address.");
        driver.close();
    }

}