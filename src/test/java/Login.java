import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.*;


public class Login extends TestBaseNG{

    @Test
    public void usuarioObligatorio() throws InterruptedException {
        Thread.sleep(1000);
        driver.findElement(userNameInput).clear();
        driver.findElement(pswInput).sendKeys("password");
        driver.findElement(logInButton).click();
        Thread.sleep(2000);
        WebElement el1 = (WebElement) driver.findElement(By.id("mat-error-0"));
        Assert.assertEquals("Username is required",el1.getText());
        Thread.sleep(1000);
    }

    @Test
    public void passObligatorio() throws InterruptedException {
        Thread.sleep(1000);
        driver.findElement(userNameInput).clear();
        driver.findElement(userNameInput).sendKeys("username");
        driver.findElement(pswInput).clear();
        driver.findElement(logInButton).click();
        Thread.sleep(2000);
        WebElement el1 = (WebElement) driver.findElement(By.id("mat-error-0"));
        Assert.assertEquals("Password is required",el1.getText());
        Thread.sleep(1000);
    }

    @Test
    public void credencialesIncorrectas() throws InterruptedException {
        Thread.sleep(1000);
        driver.findElement(userNameInput).clear();
        driver.findElement(userNameInput).sendKeys("halons");
        Thread.sleep(1000);
        driver.findElement(pswInput).clear();
        driver.findElement(pswInput).sendKeys("password");
        driver.findElement(logInButton).click();
        Thread.sleep(200);
        WebElement el1 = (WebElement) driver.findElement(By.xpath("//simple-snack-bar"));
        String a = el1.getText();
        Assert.assertEquals("CdrUsuario o contraseña incorrectos\n" +
                "X",a);
        Thread.sleep(4000);
    }

    @Test
    public void loginTest() throws Exception {
        Thread.sleep(1000);
        driver.findElement(userNameInput).clear();
        driver.findElement(userNameInput).sendKeys(username);
        Thread.sleep(1000);
        driver.findElement(pswInput).clear();
        driver.findElement(pswInput).sendKeys(password);
        driver.findElement(logInButton).click();
        WebDriverWait wait = new WebDriverWait(driver,20);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("example-h2")));
        WebElement el1 = (WebElement) driver.findElement(By.className("example-h2"));
        Assert.assertEquals("Detalles del cliente",el1.getText());
        String a = el1.getText();
        System.out.println(a);
        Thread.sleep(2000);
    }

    //@Test
    public void mensajeBienvenida() throws Exception {
        login();
        WebElement el1 = (WebElement) driver.findElement(By.xpath("//body/app[1]/menu[1]/div[1]/mat-sidenav-container[1]/mat-sidenav-content[1]/mat-toolbar[1]/mat-sidenav[1]/div[1]/mat-toolbar[1]"));
        System.out.println(el1.getText());
    }

}