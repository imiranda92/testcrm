import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class Buscador extends TestBaseNG {

    @Test // Busca al cliente "JUAN DAVID"
    public void busquedaNombre() throws InterruptedException  {
        String buscarCliente = "Juan david";
        login();
        WebElement el1 = (WebElement) driver.findElement(By.xpath("//*[@id=\"mat-tab-label-0-0\"]/div[1]"));
        el1.click();
        WebElement el2 = (WebElement) driver.findElement(By.id("mat-input-2"));
        el2.sendKeys(buscarCliente);
        Thread.sleep(2000);
        WebElement el3 = (WebElement) driver.findElement(By.xpath("//*[@id=\"mat-tab-content-0-0\"]/div[1]/div[1]/div[1]/div[5]/div[1]/button[1]"));
        el3.click();
        Thread.sleep(4000);
        WebElement el4 = (WebElement) driver.findElement(By.xpath("//*[@id=\"mat-tab-content-0-0\"]/div[1]/div[2]/table[1]/tbody[1]/mat-row[1]/mat-cell[2]"));
        String clienteEncontrado = el4.getText();
        Assert.assertTrue(clienteEncontrado.contains(buscarCliente.toUpperCase()));
        Thread.sleep(3000);
    }


}