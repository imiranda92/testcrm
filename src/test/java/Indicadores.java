import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;

import static org.testng.Assert.*;

public class Indicadores extends TestBaseNG {

    @Test
    public void clientesCreados() throws InterruptedException {
        login();
        driver.navigate().refresh();
        Thread.sleep(1500);
        WebElement el1 = (WebElement) driver.findElement(By.xpath("//*[@class=\"mat-icon notranslate material-icons mat-icon-no-color\"]"));
        el1.click();
        Thread.sleep(1000);
        WebElement el2 = (WebElement) driver.findElement(By.xpath("//*[text()='Indicadores/Dashboards ']"));
        el2.click();
        Thread.sleep(1000);
        WebElement el3 = (WebElement) driver.findElement(By.xpath("//*[text()='Clientes creados']"));
        el3.click();
        Thread.sleep(1500);
        ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
        driver.switchTo().window(tabs.get(1));

        WebDriverWait wait = new WebDriverWait(driver,20);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[text()='Semanal']")));

        WebElement el5 = (WebElement) driver.findElement(By.xpath("//*[text()='Semanal']"));
        el5.click();
        Thread.sleep(1500);
        WebElement el6 = (WebElement) driver.findElement(By.xpath("//*[text()='Escalonado Catorcenal']"));
        el6.click();
        Thread.sleep(1500);
        WebElement el7 = (WebElement) driver.findElement(By.xpath("//*[text()='Escalonado Semanal']"));
        el7.click();
        Thread.sleep(1500);
        WebElement el4 = (WebElement) driver.findElement(By.xpath("//*[text()='Catorcenal']"));
        el4.click();
        Thread.sleep(1500);
    }

    @Test
    public void estatusActuales() throws InterruptedException {
        login();
        driver.navigate().refresh();
        Thread.sleep(1500);
        WebElement el1 = (WebElement) driver.findElement(By.xpath("//*[@class=\"mat-icon notranslate material-icons mat-icon-no-color\"]"));
        el1.click();
        Thread.sleep(1000);
        WebElement el2 = (WebElement) driver.findElement(By.xpath("//*[text()='Indicadores/Dashboards ']"));
        el2.click();
        Thread.sleep(1000);
        WebElement el3 = (WebElement) driver.findElement(By.xpath("//*[text()='Estatus actuales']"));
        el3.click();
        Thread.sleep(1500);
        ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
        driver.switchTo().window(tabs.get(1));
        WebDriverWait wait = new WebDriverWait(driver,20);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@class='ScalarValue cursor-pointer text-brand-hover']")));

        WebElement el0 = (WebElement) driver.findElement(By.xpath("//*[@class='ScalarValue cursor-pointer text-brand-hover']"));
        int numTotal = Integer.parseInt(el0.getText().replaceAll(",",""));
        Thread.sleep(1500);
        WebElement el5 = (WebElement) driver.findElement(By.xpath("//*[text()='Partner']"));
        el5.click();
        Thread.sleep(1000);
        WebElement el6 = (WebElement) driver.findElement(By.xpath("//*[text()='LUMBRERA']"));
        el6.click();
        Thread.sleep(1500);
        WebElement el7 = (WebElement) driver.findElement(By.xpath("//*[text()='Add filter']"));
        el7.click();
        Thread.sleep(1000);
        WebElement el10 = (WebElement) driver.findElement(By.xpath("//*[@class='ScalarValue cursor-pointer text-brand-hover']"));
        int numFinal = Integer.parseInt(el10.getText().replaceAll(",",""));
        Assert.assertTrue(numFinal<numTotal);
    }

}