import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

import static org.testng.Assert.*;

public class TestBaseNG {

    public WebDriver driver;
    public String username="halonso@tdaserver.com";
    public String password="password";
    //locators
    By userNameInput = By.id("mat-input-0");
    By pswInput = By.id("mat-input-1");
    By logInButton = By.xpath("//button");

    @BeforeMethod
    public void setUp() throws Exception {

        String path = System.getProperty("user.dir");
        System.setProperty("webdriver.chrome.driver",path+"/src/main/resources/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("http://cdr.development.labstda.com");
    }

    @AfterMethod
    public void tearDown() throws Exception {
        driver.quit();
    }

    ///////////////////////
    //Login
    public void login() throws InterruptedException {
        Thread.sleep(1000);
        driver.findElement(userNameInput).sendKeys(username);
        Thread.sleep(1000);
        driver.findElement(pswInput).sendKeys(password);
        driver.findElement(logInButton).click();
        WebDriverWait wait = new WebDriverWait(driver,20);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("example-h2")));
        Thread.sleep(300);
    }

    //Droplist
    public Boolean dropList(String menu, String elemento, String[] otrosElem) throws InterruptedException{
        Boolean respuesta;
        login();
        WebElement  btnIteraciones = driver.findElement(By.xpath("//i[contains(text(),'streetview')]"));
        btnIteraciones.click();
        WebDriverWait wait = new WebDriverWait(driver,20);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[text()='"+menu+"']")));
        WebElement contacto = driver.findElement(By.xpath("//span[text()='"+menu+"']"));
        contacto.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[contains(text(),'"+elemento+"')]")));
        WebElement llamada = driver.findElement(By.xpath("//span[contains(text(),'"+elemento+"')]"));
        llamada.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//text()[.='Buscar']/ancestor::button[1]")));
        WebElement buscar = driver.findElement(By.xpath("//text()[.='Buscar']/ancestor::button[1]"));
        buscar.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//td[text()='"+elemento+"']")));
        if (existsElement("xpath","//*[@class=\"mat-spinner mat-progress-spinner mat-primary mat-progress-spinner-indeterminate-animation ng-star-inserted\"]")){
            WebElement loading = driver.findElement(By.xpath("//*[@class=\"mat-spinner mat-progress-spinner mat-primary mat-progress-spinner-indeterminate-animation ng-star-inserted\"]"));
            wait.until(ExpectedConditions.invisibilityOf(loading));
        }
        Thread.sleep(500);
        //Valida si existe algun resultado con "Salida"
        if(existsElement("xpath","//td[text()='"+elemento+"']")){
            respuesta=true;
            for (int i = 0; i < otrosElem.length; i++) {
                if (existsElement("xpath","//td[text()='"+otrosElem[i]+"']")){
                    respuesta = false;
                }
            }

        }else {
            respuesta=false;
        }

        return respuesta;
    }


    //Set fecha Calendario, recibe como parametro el número de calendario (inicia en cero)

    public void setFecha(int numCalendario,int año,String mes,int dia) throws InterruptedException{
        WebDriverWait wait = new WebDriverWait(driver,20);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@class='mat-datepicker-toggle-default-icon ng-star-inserted']")));
        List<WebElement> calendarios = driver.findElements(By.xpath(("//*[@class='mat-datepicker-toggle-default-icon ng-star-inserted']")));
        //Fecha Inicio
        calendarios.get(numCalendario).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@class='mat-calendar-arrow']")));
        WebElement calendarArrow = driver.findElement(By.xpath("//*[@class='mat-calendar-arrow']"));
        calendarArrow.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[text()='"+año+"']")));
        WebElement añoBton = driver.findElement(By.xpath("//div[text()='"+año+"']"));
        añoBton.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[text()='"+mes+"']")));
        WebElement mesBtn = driver.findElement(By.xpath("//div[text()='"+mes+"']"));
        mesBtn.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[text()='"+dia+"']")));
        WebElement diaBtn = driver.findElement(By.xpath("//div[text()='"+dia+"']"));
        diaBtn.click();
        Thread.sleep(800);
    }

    //Validar si un elemento existe
    public boolean existsElement(String type,String string ) {
        if (type == "id") {
            try {
                driver.findElement(By.id(string));
            } catch (NoSuchElementException e) {
                return false;
            }
        }

        if (type == "xpath") {
            try {
                driver.findElement(By.xpath(string));
            } catch (NoSuchElementException e) {
                return false;
            }
        }
        return true;
    }


}