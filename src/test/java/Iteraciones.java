import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;

import static org.testng.Assert.*;

public class Iteraciones extends TestBaseNG{

    @Test
    public void busquedaGral() throws InterruptedException{
        login();
        Thread.sleep(300);
        WebElement  btnIteraciones = driver.findElement(By.xpath("//i[contains(text(),'streetview')]"));
        btnIteraciones.click();
        Thread.sleep(800);
        WebDriverWait wait = new WebDriverWait(driver,20);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//text()[.='Buscar']/ancestor::button[1]")));
        WebElement buscar = driver.findElement(By.xpath("//text()[.='Buscar']/ancestor::button[1]"));
        Thread.sleep(800);
        buscar.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//th[text()='1']")));
        if (existsElement("xpath","//*[@class=\"mat-spinner mat-progress-spinner mat-primary mat-progress-spinner-indeterminate-animation ng-star-inserted\"]")){
            WebElement loading = driver.findElement(By.xpath("//*[@class=\"mat-spinner mat-progress-spinner mat-primary mat-progress-spinner-indeterminate-animation ng-star-inserted\"]"));
            wait.until(ExpectedConditions.invisibilityOf(loading));
        }
        Thread.sleep(500);
        //Valida si existe algun resultado
        Assert.assertTrue(existsElement("xpath","//th[text()='1']"));
    }

    @Test
    public void contactoSalida() throws InterruptedException{
        String[] otrosElem={"Entrada"};
        Assert.assertTrue(dropList("Contacto","Salida",otrosElem));
    }

   // @Test
    public void medioLlamada() throws InterruptedException{
        String[] otrosElem={"Whatsapp","Mail","SMS"};
        Assert.assertTrue(dropList("Medio","Llamada",otrosElem));
    }

    @Test
    public void fecha() throws InterruptedException{
        Boolean respuesta;
        int año=2019;
        login();
        WebElement  btnIteraciones = driver.findElement(By.xpath("//i[contains(text(),'streetview')]"));
        btnIteraciones.click();
        WebDriverWait wait = new WebDriverWait(driver,20);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@class='mat-datepicker-toggle-default-icon ng-star-inserted']")));
        List<WebElement> calendarios = driver.findElements(By.xpath(("//*[@class='mat-datepicker-toggle-default-icon ng-star-inserted']")));
        //Fecha Inicio
        setFecha(0,año,"JAN",1);

        //Fecha Fin
        setFecha(1,año,"DEC",31);

        Thread.sleep(300);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//text()[.='Buscar']/ancestor::button[1]")));
        WebElement buscar = driver.findElement(By.xpath("//text()[.='Buscar']/ancestor::button[1]"));
        buscar.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//td[contains(text(),'/"+año+"')]")));
        if (existsElement("xpath","//*[@class=\"mat-spinner mat-progress-spinner mat-primary mat-progress-spinner-indeterminate-animation ng-star-inserted\"]")){
            WebElement loading = driver.findElement(By.xpath("//*[@class=\"mat-spinner mat-progress-spinner mat-primary mat-progress-spinner-indeterminate-animation ng-star-inserted\"]"));
            wait.until(ExpectedConditions.invisibilityOf(loading));
        }
        Thread.sleep(5000);

        //resultados
        List<WebElement> resultados = driver.findElements(By.xpath("//td[contains(text(),'/')]"));
        resultados.get(0).click();
        for (int i = 0; i < resultados.size() ; i++) {
            int length = resultados.get(i).getText().length();
            if (Integer.parseInt(resultados.get(i).getText().substring(length-4))!=año){
                Assert.assertTrue(false);
            }
        }
        Assert.assertTrue(true);
    }


}