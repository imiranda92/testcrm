import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class ComparTfonNumCel {

    private WebDriver driver;
    //locators
    By btnMexico = By.xpath("//img[@src='flags/flag-mexico.png']");
    By numCelular = By.id("btn");
    By msgCodigo = By.xpath("//*[contains(text(), '<#> Tu codigo')]");
    

    @Test // busca un número de méxico
    public void buscaNumero() throws InterruptedException  {
        System.setProperty("webdriver.chrome.driver", "/Users/gesfor-g0088/Desktop/pruebas-crm/src/main/resources/chromedriver");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://www.receive-sms-online.info/");

        Thread.sleep(3000);
        WebElement el1 = (WebElement) driver.findElement(btnMexico);
        System.out.println(el1.getText());
        el1.click();
        Thread.sleep(3000);
        WebElement el2 = (WebElement) driver.findElement(numCelular);
        System.out.println(el2.getText());
        String numCelVirtual = el2.getText().substring(el2.getText().length()-10);
        System.out.println(numCelVirtual);
        Thread.sleep(3000);
    }


}